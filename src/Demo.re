
[@bs.module] external leftPad : string => int => string = "left-pad";
[@bs.module "xstate"] external machine : string => int => string = "Machine";
[@bs.module "../utils/src/index"] external thisIsFunction : string => string = "thisIsFunction";
[@bs.module "../utils/src/index"] external wraps : string => string = "wraps";

Js.log(thisIsFunction("jeejee"));

Js.log(wraps("green"));

let mac_desc = [%raw {|
    {
        key: 'light',
        initial: 'green',
        states: {
          green: {
            on: {
              TIMER: 'yellow',
            }
          },
          yellow: {
            on: {
              TIMER: 'red',
            }
          },
          red: {
            on: {
              TIMER: 'green',
            }
          }
        }
      }
    |}];

/* let mac = machine(mac_desc); */
/* Js.log(mac); */

/* let nextState: string = [%raw {| 
    mac.transition("green", "TIMER").value 
    |}];

Js.log(nextState); */

let root = leftPad("hih", 10);

let l = [1,2,3];
let a  = Array.of_list(l);
Js.logMany([|a|]);

Js.log(root ++ " hello");

let s = {js|äöü|js};

Js.log(s);
