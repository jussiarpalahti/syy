
import { Machine } from 'xstate';

const lightMachine = Machine({
  key: 'light',
  initial: 'green',
  states: {
    green: {
      on: {
        TIMER: 'yellow',
      }
    },
    yellow: {
      on: {
        TIMER: 'red',
      }
    },
    red: {
      on: {
        TIMER: 'green',
      }
    }
  }
});

export function wraps(currentState) {

    let state = currentState ? currentState : "green";
    
    const nextState = lightMachine
        .transition(currentState, 'TIMER')
        .value;
    
    return nextState;
}

export function thisIsFunction(value:string):string {
    return value + " my value";
}
