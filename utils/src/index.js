"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const xstate_1 = require("xstate");
const lightMachine = xstate_1.Machine({
    key: 'light',
    initial: 'green',
    states: {
        green: {
            on: {
                TIMER: 'yellow',
            }
        },
        yellow: {
            on: {
                TIMER: 'red',
            }
        },
        red: {
            on: {
                TIMER: 'green',
            }
        }
    }
});
function wraps(currentState) {
    let state = currentState ? currentState : "green";
    const nextState = lightMachine
        .transition(currentState, 'TIMER')
        .value;
    return nextState;
}
exports.wraps = wraps;
function thisIsFunction(value) {
    return value + " my value";
}
exports.thisIsFunction = thisIsFunction;
