FROM node:10

LABEL NAME "Contained reason"

# Sourced a bit from:
# https://github.com/kdkeyser/reasonml-docker/blob/3afd924372375e5e3cd37ff266d4900de58fac7d/dev-docker/Dockerfile

RUN groupadd -g 1000 developer && useradd -u 1000 -g 1000 developer
RUN echo "developer:developer" | chpasswd 
RUN adduser developer sudo 
RUN mkdir /home/developer
RUN chown developer /home/developer

RUN npm install -g --unsafe-perm reason-cli@3.2.0-linux
RUN npm install -g --unsafe-perm bs-platform

USER developer
